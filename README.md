# README #

This is a repository containing the phylogeo library and all necessary (and only the necessary) libraries needed to run it, enabling a relatively clean workspace. packrat is used to achieve this.

## References ##

* [phylogeo](http://zachcp.github.io/phylogeo/)
* [RStudio](https://www.rstudio.com/)
* [Packrat](https://rstudio.github.io/packrat/)